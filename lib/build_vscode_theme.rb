def build_vscode_theme(colors, name)
  content = %{{
	"name": "#{name}",
	"type": "#{colors["mode"]}",
	"tokenColors": [
		{
			"name": "Comments",
			"scope": [
				"comment",
				"punctuation.definition.comment"
			],
			"settings": {
				"foreground": "#{colors[colors["comment"]]}"
			}
		},
		{
			"name": "Strings",
			"scope": [
				"string",
				"string.regexp",
				"constant.other.symbol"
			],
			"settings": {
				"foreground": "#{colors[colors["string"]]}"
			}
		},
		{
			"name": "Strings: Escape Sequences",
			"scope": "constant.character.escape",
			"settings": {
				"foreground": "#{colors[colors["fg_space"]]}"
			}
		},
		{
			"name": "Numbers, Characters",
			"scope": [
				"constant.numeric",
				"constant.character",
				"constant.keyword",
				"constant"
			],
			"settings": {
				"foreground": "#{colors["fg_main"]}"
			}
		},
		{
			"name": "Global definitions",
			"scope": "entity.name",
			"settings": {
				"foreground": "#{colors[colors["keyword"]]}"
			}
		},
		{
			"name": "Punctuation",
			"scope": "punctuation",
			"settings": {
				"foreground": "#{colors["fg_dim"]}"
			}
		},
        {
            "name": "Variable",
            "scope": ["variable", "entity.name.variable"],
            "settings": {
                "foreground": "#{colors[colors["variable"]]}"
            }
        },
        {
            "name": "Language variable",
            "scope": "variable.language",
            "settings": {
                "foreground": "#{colors[colors["variable"]]}"
            }
        },
        {
            "name": "Constant",
            "scope": "constant",
            "settings": {
                "foreground": "#{colors[colors["constant"]]}"
            } 
        },
        {
            "name": "Built-in constant",
            "scope": [
                "constant.language",
                "punctuation.definition.constant",
                "variable.other.constant"
            ],
            "settings": {
                "foreground": "#{colors[colors["builtin"]]}"
            }
        },
        {
            "name": "Boolean",
            "scope": "constant.language.boolean",
            "settings": {
                "foreground": "#{colors[colors["constant"]]}"
            }
        },
        {
            "name": "Other",
            "scope": [
                "storage.type.type",
                "support.type.primitive",
                "support.class"
            ],
            "settings": {
                "foreground": "#{colors[colors["type"]]}"
            }
        },
        {
            "name": "Other",
            "scope": [
                "storage.modifier.async",
                "storage.type"
            ],
            "settings": {
                "foreground": "#{colors[colors["keyword"]]}"
            }
        },
        {
            "name": "Other",
            "scope": [
                "entity.other.attribute-name"
            ],
            "settings": {
                "foreground": "#{colors["fg_main"]}"
            }
        },
        {
            "name": "Function",
            "scope": ["entity.name.function", "variable.function"],
            "settings": {
                "foreground": "#{colors[colors["fnname"]]}"
            }
        },
        {
            "name": "Keywords",
            "scope": ["keyword", "keyword.control"],
            "settings": {
                "foreground": "#{colors[colors["keyword"]]}"
            }
        },
		{
			"name": "Invalid",
			"scope": "invalid",
			"settings": {
				"background": "#{colors["bg_err"]}",
				"foreground": "#{colors["fg_main"]}"
			}
		}
	],
	"colors": {
		"editor.background": "#{colors["bg_main"]}",
		"editor.foreground": "#{colors["fg_main"]}",
		"editor.lineHighlightBackground": "#{colors["bg_hl_line"]}",
		"editor.wordHighlightBackground": "#{colors["bg_region_intense"]}",
		"editor.selectionBackground": "#{colors["bg_region"]}",
		"editorWidget.background": "#{colors["bg_alt"]}",
		"editorWidget.foreground": "#{colors["fg_mode_line"]}",

		"sideBar.background": "#{colors["bg_main"]}",
		"sideBar.foreground": "#{colors["fg_main"]}",
		"sideBar.border": "#{colors["bg_alt"]}",
		"sideBarSectionHeader.background": "#{colors["bg_dim"]}",
		"sideBarSectionTitle.foreground": "#{colors["fg_dim"]}",

		"input.background": "#{colors["bg_active"]}",
		"input.foreground": "#{colors["fg_main"]}",

		"gitDecoration.addedResourceForeground": "#{colors["fg_added"]}",
		"gitDecoration.modifiedResourceForeground": "#{colors["fg_changed"]}",
		"gitDecoration.deletedResourceForeground": "#{colors["fg_removed"]}",

		"editorGroupHeader.tabsBackground": "#{colors["bg_inactive"]}",
		"tab.activeBackground": "#{colors["bg_main"]}",
		"tab.activeForeground": "#{colors["fg_main"]}",
		"tab.inactiveBackground": "#{colors["bg_inactive"]}",
		"tab.inactiveForeground": "#{colors["fg_dim"]}",

		"activityBar.background": "#{colors["bg_main"]}",
		"activityBar.foreground": "#{colors["fg_main"]}",
		"activityBar.border": "#{colors["bg_alt"]}",
		"activityBarBadge.background": "#{colors["bg_mode_line"]}",
		"activityBarBadge.foreground": "#{colors["fg_mode_line"]}",

		"editorLineNumber.foreground": "#{colors["fg_dim"]}",
		"editorCursor.foreground": "#{colors["cursor"]}",
		"editor.findMatchBackground": "#{colors["bg_yellow_intense"]}",
		"editor.findMatchHighlightBackground": "#{colors["bg_yellow_subtle"]}",

		"notifications.background": "#{colors["bg_mode_line"]}",
		"notifications.foreground": "#{colors["fg_mode_line"]}",
		"notifications.border": "#{colors["bg_mode_line"]}",
		"notificationCenterHeader.foreground": "#{colors["fg_mode_line"]}",

		"statusBar.background": "#{colors["bg_mode_line"]}",
		"statusBar.foreground": "#{colors["fg_mode_line"]}",
		"statusBar.debuggingBackground": "#{colors["bg_mode_line"]}",
		"statusBar.debuggingForeground": "#{colors["fg_mode_line"]}",
		"statusBar.noFolderBackground": "#{colors["bg_mode_line"]}",
		"statusBar.noFolderForeground": "#{colors["fg_mode_line"]}",
		"statusBarItem.remoteBackground": "#{colors["bg_mode_line"]}",
		"statusBarItem.remoteForeground": "#{colors["fg_mode_line"]}",

		"scrollbarSlider.background": "#{colors["bg_dim"]}",
		"scrollbarSlider.hoverBackground": "#{colors["bg_dim"]}",
		"scrollbarSlider.activeBackground": "#{colors["bg_alt"]}",

		"list.activeSelectionBackground": "#{colors["bg_active"]}",
		"list.activeSelectionForeground": "#{colors["fg_main"]}",
		"list.inactiveSelectionBackground": "#{colors["bg_inactive"]}",
		"list.focusHighlightForeground": "#{colors["bg_mode_line"]}",

		"quickInputList.focusForeground": "#{colors["fg_main"]}",
		"quickInputList.focusBackground": "#{colors["bg_dim"]}",

		"editorSuggestWidget.selectedBackground": "#{colors["bg_dim"]}",
		"editorSuggestWidget.focusHighlightForeground": "#{colors["bg_main"]}",
		"editorSuggestWidget.highlightForeground": "#{colors["bg_main"]}",
		"editorSuggestWidget.selectedForeground": "#{colors["bg_main"]}",

		"terminal.ansiWhite": "#{colors["fg_main"]}",
		"terminal.ansiBlack": "#{colors["bg_dim"]}",
		"terminal.ansiBlue": "#{colors["blue"]}",
		"terminal.ansiCyan": "#{colors["cyan"]}",
		"terminal.ansiGreen": "#{colors["green"]}",
		"terminal.ansiMagenta": "#{colors["magenta"]}",
		"terminal.ansiRed": "#{colors["red"]}",
		"terminal.ansiYellow": "#{colors["yellow"]}",
		"terminal.ansiBrightWhite": "#{colors["fg_dim"]}",
		"terminal.ansiBrightBlack": "#{colors["bg_active"]}",
		"terminal.ansiBrightBlue": "#{colors["blue_bright"]}",
		"terminal.ansiBrightCyan": "#{colors["cyan_bright"]}",
		"terminal.ansiBrightGreen": "#{colors["green_bright"]}",
		"terminal.ansiBrightMagenta": "#{colors["magenta_bright"]}",
		"terminal.ansiBrightRed": "#{colors["red_bright"]}",
		"terminal.ansiBrightYellow": "#{colors["yellow_bright"]}"
	}
}
}
  return content
end
