def to_kak_color(color)
  return "" unless color
  return color.gsub("#", "rgb:")
end

def build_kakoune_theme(colors, name)
  content = %{# #{name} theme for Kakoune

# Color palette
# declare-option str black default
declare-option str bg_main '#{to_kak_color(colors["bg_main"])}'
declare-option str fg_main '#{to_kak_color(colors["fg_main"])}'
declare-option str bg_dim '#{to_kak_color(colors["bg_dim"])}'
declare-option str fg_dim '#{to_kak_color(colors["fg_dim"])}'
declare-option str bg_alt '#{to_kak_color(colors["bg_alt"])}'
declare-option str fg_alt '#{to_kak_color(colors["fg_alt"])}'
declare-option str bg_active '#{to_kak_color(colors["bg_active"])}'
declare-option str bg_inactive '#{to_kak_color(colors["bg_inactive"])}'
declare-option str red '#{to_kak_color(colors["red"])}'
declare-option str red_bright '#{to_kak_color(colors["red_bright"])}'
declare-option str green '#{to_kak_color(colors["green"])}'
declare-option str green_bright '#{to_kak_color(colors["green_bright"])}'
declare-option str yellow '#{to_kak_color(colors["yellow"])}'
declare-option str yellow_bright '#{to_kak_color(colors["yellow_bright"])}'
declare-option str blue '#{to_kak_color(colors["blue"])}'
declare-option str blue_bright '#{to_kak_color(colors["blue_bright"])}'
declare-option str purple '#{to_kak_color(colors["magenta"])}'
declare-option str purple_bright '#{to_kak_color(colors["magenta_bright"])}'
declare-option str bg_mode_line '#{to_kak_color(colors["bg_mode_line"])}'
declare-option str fg_mode_line '#{to_kak_color(colors["fg_mode_line"])}'
declare-option str cursor '#{to_kak_color(colors["cursor"])}'
declare-option str bg_hl_line '#{to_kak_color(colors["bg_hl_line"])}'
declare-option str fg_space '#{to_kak_color(colors[colors["fg_space"]])}'

declare-option str psel '#{to_kak_color(colors["bg_region"])}'
declare-option str ssel '#{to_kak_color(colors["bg_hl_line"])}'

# Reference
# https://github.com/mawww/kakoune/blob/master/colors/default.kak
# For code
set-face global value '#{to_kak_color(colors["fg_main"])}'
set-face global type '#{to_kak_color(colors[colors["type"]])}'
set-face global variable '#{to_kak_color(colors[colors["variable"]])}'
set-face global keyword '#{to_kak_color(colors[colors["keyword"]])}'
set-face global module '#{to_kak_color(colors["fg_main"])}'
set-face global function '#{to_kak_color(colors[colors["fnname"]])}'
set-face global string '#{to_kak_color(colors[colors["string"]])}'
set-face global builtin '#{to_kak_color(colors[colors["builtin"]])}'
set-face global constant '#{to_kak_color(colors[colors["constant"]])}'
set-face global comment '#{to_kak_color(colors[colors["comment"]])}'
set-face global meta '#{to_kak_color(colors[colors["preprocessor"]])}'

set-face global operator '#{to_kak_color(colors["fg_main"])}'
set-face global comma '#{to_kak_color(colors["fg_main"])}'
set-face global bracket '#{to_kak_color(colors["bg_paren"])}'

# For markup
set-face global title "%opt{purple}"
set-face global header "%opt{yellow_bright}"
set-face global bold "%opt{purple}"
set-face global italic "%opt{purple_bright}"
set-face global mono "%opt{green}"
set-face global block "%opt{blue_bright}"
set-face global link "%opt{green}"
set-face global bullet "%opt{green}"
set-face global list "%opt{fg_main}"

# Builtin faces
set-face global Default "%opt{fg_main},%opt{bg_main}"
set-face global PrimarySelection "default,%opt{psel}"
set-face global SecondarySelection "default,%opt{ssel}"
set-face global PrimaryCursor "%opt{bg_main},%opt{cursor}"
set-face global SecondaryCursor "%opt{bg_main},%opt{fg_alt}"
set-face global PrimaryCursorEol "%opt{bg_main},%opt{red_bright}"
set-face global SecondaryCursorEol "%opt{bg_main},%opt{blue}"
set-face global LineNumbers "%opt{fg_dim},%opt{bg_main}"
set-face global LineNumberCursor "%opt{fg_alt},%opt{bg_main}+b"
set-face global LineNumbersWrapped "%opt{bg_dim},%opt{bg_main}+i"
set-face global MenuForeground "%opt{bg_main},%opt{fg_main}+b"
set-face global MenuBackground "%opt{fg_main},%opt{bg_alt}"
set-face global MenuInfo "%opt{fg_alt},%opt{bg_alt}"
set-face global Information "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global Error "%opt{red},%opt{bg_mode_line}"
set-face global StatusLine "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global StatusLineMode "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global StatusLineInfo "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global StatusLineValue "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global StatusCursor "%opt{fg_main},%opt{blue}"
set-face global Prompt "%opt{fg_mode_line},%opt{bg_mode_line}"
set-face global MatchingChar "%opt{blue},%opt{bg_main}"
set-face global Whitespace "%opt{fg_space},%opt{bg_main}+f"
set-face global WrapMarker Whitespace
set-face global BufferPadding "%opt{bg_main},%opt{bg_main}"
}
  return content
end
