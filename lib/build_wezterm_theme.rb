def build_wezterm_theme(colors, name)
  ef_base_url = "https://git.sr.ht/~protesilaos/ef-themes/tree/main/item";
  author = 'anhsirk0';

  content = %{[colors]
ansi = [
    '#{colors["bg_dim"]}',
    '#{colors["red"]}',
    '#{colors["green"]}',
    '#{colors["yellow"]}',
    '#{colors["blue"]}',
    '#{colors["magenta"]}',
    '#{colors["cyan"]}',
    '#{colors['fg_main']}'
]
brights = [
    '#{colors["bg_active"]}',
    '#{colors["red_bright"]}',
    '#{colors["green_bright"]}',
    '#{colors["yellow_bright"]}',
    '#{colors["blue_bright"]}',
    '#{colors["magenta_bright"]}',
    '#{colors["cyan_bright"]}',
    '#{colors['fg_dim']}'
]
cursor_bg = '#{colors["cursor"]}'
cursor_border = '#{colors["cursor"]}'
cursor_fg = '#{colors["bg_main"]}'
background = '#{colors["bg_main"]}'
foreground = '#{colors["fg_main"]}'
selection_bg = '#{colors["bg_region"]}'
selection_fg = '#{colors["fg_main"]}'

[metadata]
name = '#{name}'
author = '#{author}'
origin_url = '#{ef_base_url}/#{name.downcase}-theme.el'
}
  return content
end
