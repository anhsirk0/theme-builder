def build_rofi_theme(colors, name)
  content = %{/* #{name} */
* {
    background:     #{colors["bg_main"]}; /* bg_main */
    background-alt: #{colors["bg_dim"]}; /* bg_dim */
    foreground:     #{colors["fg_main"]}; /* fg_main */
    selected:       #{colors["bg_mode_line"]}; /* bg_mode_line */
    selected-fg:    #{colors["fg_mode_line"]}; /* fg_mode_line */
    active:         #{colors["green"]}; /* g_green */
    urgent:         #{colors["red"]}; /* red */
}
}
  return content
end
