def build_awesome_theme(colors, name)
  content = %{-- Colors #{name}

local colors = {}
colors.wallpaper     = "#{colors["bg_dim"]}"
colors.bg_main       = "#{colors["bg_main"]}"
colors.fg_main       = "#{colors["fg_main"]}"
colors.bg_dim        = "#{colors["bg_dim"]}"
colors.fg_dim        = "#{colors["fg_dim"]}"
colors.bg_alt        = "#{colors["bg_alt"]}"
colors.fg_alt        = "#{colors["fg_alt"]}"
colors.bg_active     = "#{colors["bg_active"]}"
colors.bg_inactive   = "#{colors["bg_inactive"]}"

colors.red           = "#{colors["red"]}"
colors.green         = "#{colors["green"]}"
colors.green_bright  = "#{colors["green_bright"]}"
colors.yellow        = "#{colors["yellow"]}"
colors.yellow_bright = "#{colors["yellow_bright"]}"
colors.blue          = "#{colors["blue"]}"
colors.blue_bright   = "#{colors["blue_bright"]}"
colors.purple        = "#{colors["magenta"]}"
colors.purple_bright = "#{colors["magenta_bright"]}"
colors.bg_accent_alt = "#{colors["bg_mode_line"]}"
colors.fg_accent_alt = "#{colors["fg_mode_line"]}"
colors.bg_accent     = "#{colors["cursor"]}"
colors.fg_accent     = "#{colors["fg_main"]}"

return colors
}
  return content
end
