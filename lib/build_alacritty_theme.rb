def build_alacritty_theme(colors, name)
  content = %{# Colors #{name}
[colors.normal]
black = '#{colors["bg_dim"]}'
red = '#{colors["red"]}'
green = '#{colors["green"]}'
yellow = '#{colors["yellow"]}'
blue = '#{colors["blue"]}'
magenta = '#{colors["magenta"]}'
cyan = '#{colors["cyan"]}'
white = '#{colors['fg_main']}'
[colors.bright]
black = '#{colors["bg_active"]}'
red = '#{colors["red_bright"]}'
green = '#{colors["green_bright"]}'
yellow = '#{colors["yellow_bright"]}'
blue = '#{colors["blue_bright"]}'
magenta = '#{colors["magenta_bright"]}'
cyan = '#{colors["cyan_bright"]}'
white = '#{colors['fg_dim']}'
[colors.cursor]
cursor = '#{colors["cursor"]}'
text = '#{colors["bg_main"]}'
[colors.primary]
background = '#{colors["bg_main"]}'
foreground = '#{colors["fg_main"]}'
[colors.selection]
background = '#{colors["bg_region"]}'
text = '#{colors["fg_main"]}'
}
  return content
end

## Old format
# def build_alacritty_theme(colors, name)
#   content = %{# Colors #{name}
# colors:
#   normal:
#     black: '#{colors["bg_dim"]}'
#     red: '#{colors["red"]}'
#     green: '#{colors["green"]}'
#     yellow: '#{colors["yellow"]}'
#     blue: '#{colors["blue"]}'
#     magenta: '#{colors["magenta"]}'
#     cyan: '#{colors["cyan"]}'
#     white: '#{colors['fg_main']}'
#   bright:
#     black: '#{colors["bg_active"]}'
#     red: '#{colors["red_bright"]}'
#     green: '#{colors["green_bright"]}'
#     yellow: '#{colors["yellow_bright"]}'
#     blue: '#{colors["blue_bright"]}'
#     magenta: '#{colors["magenta_bright"]}'
#     cyan: '#{colors["cyan_bright"]}'
#     white: '#{colors['fg_dim']}'
#   cursor:
#     cursor: '#{colors["cursor"]}'
#     text: '#{colors["bg_main"]}'
#   primary:
#     background: '#{colors["bg_main"]}'
#     foreground: '#{colors["fg_main"]}'
#   selection:
#     background: '#{colors["bg_region"]}'
#     text: '#{colors["fg_main"]}'
# }
#   return content
# end

