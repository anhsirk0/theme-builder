require_relative 'builder'

def read_colors(file_name)
  unless File.file?(file_name)
    puts "File does not exists '#{file_name}'"
    return false
  end

  puts "\nReading '#{file_name}' ...\n"

  colors = Hash.new
  File.readlines(file_name).each_with_index do |line, index|
    if index == 0
      colors["mode"] = line.include?("light") ? "light" : "dark"
    end
    line = line.strip
    next unless line.start_with?("(")
    line = line.gsub(/(\(|\)|")/, '').gsub('-', '_').gsub('cooler', 'bright')
    next if line.empty?
    # next unless line.match(/[a-z_]+\s+#[a-zA-Z0-9]{6}/)
    # next if line.match(/(faint|add|remove|change|hover|warmer|subtle)/)
    name, hex = line.split
    next if (name.empty? && hex.empty?)
    colors[name] = hex
  end

  colors["bg_mode_line"] ||= colors["bg_mode_line_active"]
  colors["cursor"] ||= colors["bg_magenta_intense"]
  colors["bg_alt"] ||= colors["bg_active"]
  colors["fg_mode_line"] ||= colors["fg_main"]
  colors["bg_paren"] ||= colors["bg_paren_match"]

  if file_name.include?("modus")
    colors["cursor"] = colors["fg_main"]
  end
  # p colors
  return colors
end

def main
  # pac_themes = ""
  ARGV.each_with_index do |theme, index|
    colors = read_colors(theme)
    next unless colors.length

    name = File.basename(theme, ".*")
    bob = Builder.new(name, colors)
    bob.build_alacritty
    bob.build_awesome
    bob.build_rofi
    bob.build_rio
    bob.build_vscode
    bob.build_kakoune
    bob.build_wezterm

    # pac_themes += %{
	# 		{
	# 			"label": "#{name}",
	# 			"uiTheme": "vs#{colors["mode"] == "dark" ? "-dark" : ""}",
	# 			"path": "./themes/#{name.downcase}.json"
	# 		}#{index == ARGV.size - 1 ? "" : ","}}
  end
#   pac_json = %{{
# 	"name": "ef-themes",
# 	"displayName": "Ef-Themes",
# 	"publisher": "anhsirk0",
# 	"engines": {
# 		"vscode": "^1.12.0"
# 	},
# 	"categories": [
# 		"Themes"
# 	],
# 	"contributes": {
# 		"themes": [#{pac_themes}
# 		]
# 	}
# }
# }
  # Builder.write('package.json', 'vscode', pac_json)
end

main
