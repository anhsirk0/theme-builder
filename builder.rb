require 'fileutils'
require_relative 'lib/build_alacritty_theme'
require_relative 'lib/build_awesome_theme'
require_relative 'lib/build_rio_theme'
require_relative 'lib/build_rofi_theme'
require_relative 'lib/build_wezterm_theme'
require_relative 'lib/build_kakoune_theme'
require_relative 'lib/build_vscode_theme'

class Builder
  def initialize(name, colors)
    @name = name.gsub(/\w+/) { |w| w.capitalize }
    @colors = colors
  end

  def build_alacritty()
    puts "Building for alacritty ..."
    content = build_alacritty_theme(@colors, @name)
    Builder.write(@name.downcase + '.toml', 'alacritty', content)
  end

  def build_awesome()
    puts "Building for awesome ..."
    content = build_awesome_theme(@colors, @name)
    name = @name.downcase
    Builder.write('colors.lua', 'awesome/' + name, content)
  end

  def build_rio()
    puts "Building for rio ..."
    content = build_rio_theme(@colors, @name)
    Builder.write(@name.downcase + '.toml', 'rio', content)
  end

  def build_rofi()
    puts "Building for rofi ..."
    content = build_rofi_theme(@colors, @name)
    Builder.write(@name.downcase + '.rasi', 'rofi', content)
  end

  def build_vscode()
    puts "Building for vscode ..."
    content = build_vscode_theme(@colors, @name)
    # pac_json = build_package_json(@name)
    
    Builder.write(@name.downcase + '.json', 'vscode/themes', content)
    # Builder.write('package.json', 'vscode', pac_json)
  end

  def build_wezterm()
    puts "Building for wezterm ..."
    content = build_wezterm_theme(@colors, @name)
    Builder.write(@name.downcase + '.toml', 'wezterm', content)
  end

  def build_kakoune()
    puts "Building for kakoune ..."
    content = build_kakoune_theme(@colors, @name)
    Builder.write(@name.downcase + '.kak', 'kakoune', content)
  end

  def self.write(name, dir, content)
    out_dir = "output/#{dir}"
    file_name = "#{out_dir}/#{name.downcase}"
    puts "Saving as '#{file_name}'\n\n"

    FileUtils.mkdir_p(out_dir) unless File.directory?(out_dir)
    File.write(file_name, content)
  end
end
